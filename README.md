Hadolint
=========

Role that installs Hadolint

Requirements
------------

None

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

Installing Hadolint on targeted machine:

    - hosts: servers
      roles:
         - hadolint

License
-------

[MIT](LICENSE)

Author Information
------------------

This role was created by [bradthebuilder](https://bradthebuilder.me)
